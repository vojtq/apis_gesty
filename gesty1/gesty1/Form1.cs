﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using Emgu.Util;
using System.Drawing.Imaging;
namespace gesty1
{
    public partial class Form1 : Form
    {
        Capture capture;

        bool drawingStarted = false;
        List<Point> pointsList = new List<Point>();
        bool saveImage = false;
        string pathToSave;
        public bool camameraActive = true;
        List<Mat> imageList;
        int currentImg = 0;
        Timer timer;
        Mat matImg1;
        Mat matImg2;
        Mat matImg3;
        public Form1()
        {
            InitializeComponent();
            capture = new Capture(); //create a camera captue
            imageList = new List<Mat>();
            timer = new Timer();
            timer.Interval = (1 * 1000); // 0,1 secs
            timer.Tick += new EventHandler(imageRefresh);
            //timer.Start();
            matImg1 = new Mat();
            matImg2 = new Mat();
            matImg3 = new Mat();
            

            Application.Idle += new EventHandler(delegate(Object sender, EventArgs e)
            {
                //image1 = capture.QueryFrame().Bitmap;
                if (camameraActive)
                {
                    matImg1 = capture.QueryFrame();
                    pictureBox1.Image = matImg1.ToImage<Bgr, Byte>().ToBitmap();

                    updateSecondPanel();
                }
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Jpeg File (*.jpeg)|*.jpeg";
            dialog.DefaultExt = "jpeg";
            dialog.AddExtension = true;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                //matImg1.Save(dialog.FileName, ImageFormat.Jpeg);
                matImg1.Save(dialog.FileName);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void imageRefresh(object sender, EventArgs e)
        {
            if(camameraActive)
            { 

            }
            if(saveImage)
            {
                matImg1.Save(pathToSave + "\\" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".jpeg");
            }
            updateSecondPanel();
        }

        // PictureBox related functions
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            SolidBrush brush = new SolidBrush(Color.Blue);

            foreach (var point in pointsList)
            {
                e.Graphics.FillRectangle(brush, point.X, point.Y, 2, 2);
            }
        }

        private void pictureBox1_MouseDown(object sender, EventArgs e)
        {
            drawingStarted = true;
        }

        private void pictureBox1_MouseMove(object sender, EventArgs e)
        {
            if (drawingStarted) drawPoint(MousePosition.X, MousePosition.Y);
        }

        private void pictureBox1_MouseUp(object sender, EventArgs e)
        {
            drawingStarted = false;
        }

        private void pictureBox1_MouseDoubleClick(object sender, EventArgs e)
        {
            pointsList.Clear();
        }
        
        private void drawPoint(int x, int y)
        {
            pointsList.Add(new Point(x, y));
            this.Invalidate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show(dialog.SelectedPath);
                pathToSave = dialog.SelectedPath;
                button4.Enabled = true;
                saveImage = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            saveImage = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            camameraActive = false;
            imageList.Clear();
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;

            dialog.Filter = "Jpeg File (*.jpeg)|*.jpeg|Jpg File (*.jpg)|*.jpg";

            
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in dialog.FileNames)
                {
                    //Image img = Image.FromFile(file);
                    Mat img = CvInvoke.Imread(file, Emgu.CV.CvEnum.LoadImageType.AnyColor);
                    imageList.Add(img);
                }
            }
            if (imageList.Count() > 0)
            {
                currentImg = 0;
                matImg1 = imageList.ElementAt(currentImg);
                pictureBox1.Image = matImg1.Bitmap;
                updateSecondPanel();
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {   
            if((currentImg -1)>= 0 )
            {
                currentImg--;
            }
            matImg1 = imageList.ElementAt(currentImg);
            pictureBox1.Image = matImg1.Bitmap;
            updateSecondPanel();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if ((currentImg +1)< imageList.Count())
            {
                currentImg++;
            }
            matImg1 = imageList.ElementAt(currentImg);
            pictureBox1.Image = matImg1.Bitmap;
            updateSecondPanel();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            camameraActive = true;
            imageList.Clear();
        }
        private void updateSecondPanel()
        {

            Image<Gray, Byte> imgGray = matImg1.ToImage<Gray, Byte>();
            Image<Bgr, Byte> img = matImg1.ToImage<Bgr, Byte>();

            //Emgu.CV.CvInvoke.CvtColor(matImg1, matImg2, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);

            //imgGray = img.InRange(new Bgr(36, 48, 64), new Bgr(75, 99, 112));
            //imgGray = img.InRange(new Bgr(30, 45, 60), new Bgr(80, 100, 115));
            //CvInvoke.Threshold(imgGray, imgGray, 45, 255, Emgu.CV.CvEnum.ThresholdType.Binary | Emgu.CV.CvEnum.ThresholdType.Otsu);
            imgGray = img.InRange(new Bgr(30, 45, 60), new Bgr(80, 100, 115));
            //Emgu.CV.CvInvoke.GaussianBlur(imgGray, imgGray, new Size(5, 5), 3);

            CvInvoke.Threshold(imgGray, imgGray, 90, 255, Emgu.CV.CvEnum.ThresholdType.Binary);

            imgGray = imgGray.Dilate(2);
            imgGray=imgGray.Erode(2);
            

            pictureBox2.Image = imgGray.ToBitmap();
            matImg2 = imgGray.Mat;
            
            //Image<Gray,Byte> imgGray = matImg2.ToImage<Gray, Byte>();tmp commented
            //pictureBox2.Image = imgGray.ToBitmap();tmp commented
            updateThirdPanel();
          
            //Mat mat = imageCV.Mat;
            //Emgu.CV.CvInvoke.CvtColor(mat, imageMat2, Emgu.CV.CvEnum.ColorConversion.Rgb2Gray);
        }
        private void updateThirdPanel()
        {
            Image<Gray, Byte> imgGray = matImg2.ToImage<Gray, Byte>();
            Image<Gray, Byte> imgGrayOrigin = matImg2.ToImage<Gray, Byte>();

            Image<Bgr, Byte> img = matImg2.ToImage<Bgr, Byte>();


            Image<Gray, Byte> imgGaussian = imgGray.Clone();

            CvInvoke.GaussianBlur(imgGray, imgGaussian, new Size(7, 7), 1.4);
            float[,] ker = { {-1, 0, 1} };
            imgGaussian.Convolution(new ConvolutionKernelF(ker));

            Image<Gray, Byte> imgCanny = imgGray.Clone();

            CvInvoke.Canny(imgGaussian, imgCanny, 10, 20);
            //CvInvoke.Threshold(imgGray, imgGray, 90, 255, Emgu.CV.CvEnum.ThresholdType.Otsu);
            //Emgu.CV.CvInvoke.Erode(imgGray,imgGray)
            Emgu.CV.Util.VectorOfVectorOfPoint contours = new Emgu.CV.Util.VectorOfVectorOfPoint();
           // Emgu.CV.Util.VectorOfVectorOfPoint hull = new Emgu.CV.Util.VectorOfVectorOfPoint();
            

            Mat hier = new Mat();
            Mat matHul = new Mat();

            CvInvoke.FindContours(imgGray, contours, hier, Emgu.CV.CvEnum.RetrType.Tree, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            Image<Bgr, byte> imgout = new Image<Bgr, byte>(imgGray.Width, imgGray.Height, new Bgr(0,0,0));

            //imgGray = matImg2.ToImage<Gray, Byte>();
            double largestArea=0;
            int largestAreaIndex = 0;
            Rectangle rect=new Rectangle();
            if (contours.Size > 0)
            {
                for (int i = 0;  i < contours.Size; i++)
                {
                    if(CvInvoke.ContourArea(contours[i])>largestArea)
                    {
                        largestAreaIndex = i;
                        largestArea = CvInvoke.ContourArea(contours[i]);
                        rect = CvInvoke.BoundingRectangle(contours[i]);
                        imgGray.ROI = rect;
                    }
                }
                // imgout.Draw(rect, new Bgr(0, 255, 0));

                //centroid
                MCvMoments moments = CvInvoke.Moments(contours[largestAreaIndex]);
                Point center = new Point((int)moments.GravityCenter.X,(int)moments.GravityCenter.Y);
                CvInvoke.Circle(imgout, center, 20, new MCvScalar(100, 0, 100));


                Emgu.CV.Util.VectorOfPoint contour = contours[largestAreaIndex];
                Emgu.CV.Util.VectorOfVectorOfInt defects = new Emgu.CV.Util.VectorOfVectorOfInt();
                Emgu.CV.Util.VectorOfPoint approxContour = new Emgu.CV.Util.VectorOfPoint();
                Emgu.CV.Util.VectorOfInt hull = new Emgu.CV.Util.VectorOfInt();
                Emgu.CV.Util.VectorOfPoint hullP = new Emgu.CV.Util.VectorOfPoint();
                Emgu.CV.Util.VectorOfPoint fingerPoints = new Emgu.CV.Util.VectorOfPoint();
                List<Point> fingerPointsList = new List<Point>();


                {
                    CvInvoke.ApproxPolyDP(contour, approxContour, 30, true);
                    CvInvoke.ConvexHull(contour, matHul, false);
                    CvInvoke.ConvexHull(contour, hullP);
                    // CvInvoke.Polylines(imgout, hullP, true, new MCvScalar(0, 255, 0), 1, Emgu.CV.CvEnum.LineType.AntiAlias);
                    //CvInvoke.Polylines(imgout, matHul, true, new MCvScalar(0, 255, 0), 1, Emgu.CV.CvEnum.LineType.AntiAlias);

                    //CvInvoke.Polylines(imgout, approxContour, true, new MCvScalar(0, 255, 0), 1, Emgu.CV.CvEnum.LineType.AntiAlias);

                    for (int i = 0; i < approxContour.Size; i++)
                    {
                        if (approxContour[i].Y < center.Y && isPartOfHul(approxContour[i], hullP))
                        {
                            fingerPointsList.Add(approxContour[i]);
                        }
                    }

                    foreach (Point p in fingerPointsList)
                    {
                        CvInvoke.Circle(imgout, p, 20, new MCvScalar(100, 255, 0));
                        CvInvoke.Line(imgout, center, p, new MCvScalar(0, 255, 255), 1);

                    }
                    fingersText.Text = fingerPointsList.Count.ToString();
                    nameGesture(fingerPointsList.Count);
                }
            }
            //pictureBox3.Image = imgGray.ToBitmap();
            

            //CvInvoke.DrawContours(imgout, contours, largestAreaIndex, new MCvScalar(255, 0, 0));

            pictureBox3.Image = imgout.ToBitmap();
        }

        private bool isPartOfHul(Point p, Emgu.CV.Util.VectorOfPoint hull)
        {
           
            for (int k= 0; k < hull.Size; k++)
            {

                if (Math.Abs(hull[k].Y-p.Y)<15 && Math.Abs(hull[k].X -p.X)<15)
                {
                    return true;
                }
            }
            return false;
        }

        private void nameGesture(int fingersCount)
        {
            if (fingersCount == 5)
            {
                gestureName.Text = "Rozpostarta dłoń";
            }
            else if (fingersCount == 1)
            {
                gestureName.Text = "Podniesiony palec";
            }
            else
            {
                gestureName.Text = "";
            }
        }
    }
}
